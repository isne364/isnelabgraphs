#include<iostream>
#include <list>
using namespace std;

class Node {
public:
	char data;
	Node *next, *prev;
	Node()
	{
		next = prev = 0;
	}
	Node(char el, Node *n = 0, Node *p = 0)
	{
		data = el; next = n; prev = p;
	}
};

class List {
public:
	List() { head = tail = 0; }
	int isEmpty() { return head == 0; }
	~List();
	void pushToTail(char el);
	char popTail();
private:
	Node *head, *tail;
};
List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}
void List::pushToTail(char el)
{
	tail = new Node(el, 0, tail);
	if (tail == 0)
	{
		tail = head;
	}
	else
	{
		tail->prev->next = tail;
	}
}
char List::popTail()
{
	if (head == tail)
	{
		delete head;
		head = tail = 0;
	}
	else
	{
		Node *tmp;
		for (tmp = head; tmp->next != tail; tmp = tmp->next);
		delete tail;
		tail = tmp;
		tail->next = 0;
	}
	return NULL;
}