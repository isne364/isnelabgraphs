#include<iostream>
#include "graph.h"
using namespace std;
int main() {
	int n;
	char letter[] = "A";
	int arr[100][100];
	List mylist;
	cout << "Input amount of nodes : ";
	cin >> n;
	cout << endl;
	for(int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			cout << "Input matrix[" << i << "]" << "[" << j << "] : ";
			cin >> arr[i][j];
		}
		cout << endl;
	}
	cout << "################################################" << endl;
	cout << "## Adjacency matrix[array] ##" << "\n\n";
	for (int b = 0; b < n; b++) {
		cout << " " << static_cast<char>(letter[0] + b);
	}
	cout << endl;
	for (int c = 0; c < n; c++) {
		cout << static_cast<char>(letter[0] + c) << " ";
		for (int b = 0; b < n; b++) {
			cout << arr[c][b] << " ";
		}
		cout << endl;
	}
	cout << "################################################" << endl;
	cout << "## Adjacency Linked list ##" << "\n\n";
	for (int m = 0; m < n; m++) {
		cout << static_cast<char>(letter[0] + m) << endl;
	}

	cout << "################################################" << endl;
	cout << "## Check the graph to answer ##" << "\n\n";
	cout << "1.Multigraph? : " << endl;
	cout << "2.Pseudograph? : " << endl;
	cout << "3.Digraph? : " << endl;
	cout << "4.Weighted graph? : " << endl;
	cout << "5.Complete graph? : " << endl;
	cout << "################################################";

	system("PAUSE");
}